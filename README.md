# Picture This

Picture This is an API for rendering PDF documents from a declarative, component-based syntax.

Use the endpoint `/v1/render` to create your PDF and see [the docs](https://gitlab.com/aybry/picture-this/-/blob/main/picture_this/v1/docs/components.md) for detailed information on the components.

## Hello World

Get going with a simple example:

```bash
curl --location --request POST 'https://picture-this.ay-bryson.com/v1/render' \
--header 'Content-Type: application/json' \
--data-raw '{"title": "Hello World", "return_as": "pdf", "components": [{"component": "heading", "content": "Hello, World!"}, {"component": "block_text", "content": "I am using picture this."}]}' > helloworld.pdf
```

## Components

## Simple Invoice

Use the address and table components to create a simple invoice.

```bash
curl --location --request POST 'https://picture-this.ay-bryson.com/v1/render' \
--header 'Content-Type: application/json' \
--data-raw '{"title": "Invoice No. 123", "description": "Invoice No. 123 dated 13 December 2022", "return_as": "pdf", "components": [{"component": "address", "lines": ["Mr Anthony Onymous", "123 Fake Straße", "12345 Berlin", "Germany"], "alignment": "right"}, {"component": "address", "lines": ["Ms Anne Onymous", "Anonymenstr. 123", "54321 München", "Germany"]}, {"component": "doc_info", "table": [["Invoice No.", "123"], ["Invoice date", "13 December 2022"], ["Billing period", "November 2022"]]}, {"component": "heading", "content": "Invoice"}, {"component": "table", "numbered_rows": true, "column_headers": [{"content": "Date"}, {"content": "Description"}, {"content": "Quantity", "data_type": "numeric"}, {"content": "Price", "data_type": "currency"}, {"content": "Subtotal", "data_type": "currency"}], "rows": [["01.11.2022", "Write a bash script that backs up prod database", "4", "100", "400"], ["03.11.2022", "Create cronjob to automatically backup database", "3", "100", "300"], ["12.11.2022", "Refill prod database with backup data after data loss", "8", "100", "800"], ["18.11.2022", "Bugfix", "1", "100", "100"]]}, {"component": "table", "table_type": "totals_table", "alignment": "right", "fill_width": false, "colour_alternate_rows": false, "show_column_header_content": false, "column_headers": [{}, {"data_type": "currency"}], "rows": [["Total net", "1600"], ["19.00% VAT", "304"], ["Total gross", "1904"]]}, {"component": "footer", "columns": [{"lines": ["Mr Anthony Onymous", "123 Fake Straße", "12345 Berlin", "Germany"]}, {"table": [["IBAN:", "DE12 3456 7890 9876 5432 10"], ["BIC:", "AAKKAABK"], ["Bank:", "Anonybank GmbH"]]}]}]}
' > invoice.pdf
```

## Add a Logo

Upload an image using the form at `https://picture-this.ay-bryson.com/v1/upload`, and save the UUID you receive in response. Then, add the logo to your component list:

<!-- TODO: Update UUID for prod system -->

```bash
curl --location --request POST 'https://picture-this.ay-bryson.com/v1/render' \
--header 'Content-Type: application/json' \
--data-raw '{"title": "A Song of A and B", "return_as": "pdf", "components": [{"component": "image", "alignment": "right", "uuid": "933c5611-773b-4c01-8b66-348e745512b6"},  {"component": "block_text", "content": "\n\b\n\b\n\b\n\b\n"}, {"component": "heading", "content": "A Story of Me and my Logo"}, {"component": "block_text", "content": "This is a logo I created with GIMP. It contains the letters \"a\" and \"b\" in a golden Ubuntu monospace font on a green background. The letters represent the initials of my double-barrel surname, the green represents the dark greens of a pine forest, while the gold represents the delicious chanterelles you might find there."}, {"component": "block_text", "content": "Actually, these things represent nothing, but it was a cool story."}, {"component": "footer", "columns": [{"lines": ["© Samuel Ay-Bryson"]}]}]}
' > story.pdf
```

# Get Started

To run Picture This on your own machine, simply clone the repository and up the containers with `docker-compose`:

```bash
docker-compose up -d
```

## Integrate into Existing Docker System

Using the example docker-compose service config, you can easily build it into your existing system. Then, assuming the containers are within the same Docker network, call Picture This at the URL `http://picture-this:8000/v1/render`.

# Roadmap

## FastAPI Migration Plan of Action

- [x] implement components as pydantic BaseModels
- [x] make pdf document supercomponent
- [x] image load into pdf
- [x] image upload media folder
- [x] ~~image upload database model & SQL~~
- [x] upload endpoint
- [x] ~~media GET endpoint?~~
- [x] render endpoint
- [x] fix CI

## Future Features

- [ ] improve docker build in CI
- [ ] autogenerated component visuals in docs
- [ ] regression tests for generated PDFs
- [ ] integration, e2e tests
