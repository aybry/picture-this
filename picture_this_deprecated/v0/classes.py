from logging import getLogger
from pathlib import Path

from django.conf import settings
from jinja2 import Environment, FileSystemLoader, select_autoescape
from weasyprint import HTML, CSS
from weasyprint.text.fonts import FontConfiguration


logger = getLogger(__name__)


class PdfTemplate(object):
    TEMPLATES_ROOT = Path("v0") / "templates"

    def __init__(self, template_name: str, data: dict):
        self.template_directory = self._validate(template_name)
        self.font_config = FontConfiguration()
        self.data = data

    def _validate(self, template_name: str):
        if not (self.TEMPLATES_ROOT / template_name).is_dir():
            raise FileNotFoundError(
                f"Directory {self.TEMPLATES_ROOT / template_name} not found"
            )
        else:
            return self.TEMPLATES_ROOT / template_name

    @property
    def jinja(self):
        env = Environment(
            loader=FileSystemLoader(settings.BASE_DIR),
            autoescape=select_autoescape(),
        )

        template_path = self.template_directory / "_main.html"
        logger.debug(f"Using template path {template_path}")

        return env.get_template(str(template_path))

    @property
    def common_css(self):
        common_css_path = settings.BASE_DIR / "project" / "static" / "common.css"
        logger.debug(f"Using fonts path {common_css_path}")

        return CSS(str(common_css_path), font_config=self.font_config)

    @property
    def style_css(self):
        style_path = self.template_directory / "_style.css"
        logger.debug(f"Using style path {style_path}")

        return CSS(str(style_path), font_config=self.font_config)

    @property
    def css(self):
        return [
            self.common_css,
            self.style_css,
        ]

    @property
    def html(self):
        logger.debug("Rendering template from data")
        rendered = self.jinja.render(data=self.data)
        logger.debug("Template rendered successfully")

        return HTML(string=rendered)

    def get_output_path(self, document_identifier: str = None):
        if document_identifier is None:
            logger.debug("Using standard document_identifier (bill number)")
            document_identifier = self.data["bill_nr"]

        return settings.PDF_OUTPUT_DIR / f"{document_identifier}.pdf"

    def write(self, document_identifier: str = None) -> Path:
        logger.debug("Creating PDF...")

        invoice_output_path = self.get_output_path(document_identifier)
        logger.debug(f"Output filepath: {invoice_output_path}")

        self.html.write_pdf(
            invoice_output_path,
            stylesheets=self.css,
            font_config=self.font_config,
        )
        logger.info(f"Created PDF: {invoice_output_path}")

        return invoice_output_path
