import json

from django.conf import settings
from django.test import TestCase, Client

from v0.views import SellerBillView


VERSION = "v0"


class SellerBillViewTestCase(TestCase):
    ENDPOINT = f"/{VERSION}/seller_bill"

    @classmethod
    def setUpTestData(cls):
        with (
            settings.BASE_DIR
            / VERSION
            / "tests"
            / "fixtures"
            / "seller_bill_examples.json"
        ).open() as examples_file:
            cls.seller_bills_examples = json.load(examples_file)

    def setUp(self):
        self.client = Client()

    def test_template_identifier(self):
        self.assertEqual(SellerBillView.TEMPLATE_IDENTIFIER, "seller_bill")

    def test_get_returns_string_response(self):
        response = self.client.get(self.ENDPOINT)

        response_data = response.json()
        self.assertIsInstance(response_data, str)
        self.assertEqual(response_data, "Please POST me your data...")

    def test_post_200(self):
        for fixture in self.seller_bills_examples[::5]:
            response = self.client.post(
                self.ENDPOINT, fixture, content_type="application/json"
            )

            response_data = response.json()
            self.assertIn("pdf/", response_data)
            self.assertIn(fixture["bill_nr"], response_data)

    def test_put_405(self):
        response = self.client.put(self.ENDPOINT, self.seller_bills_examples[0])
        self.assertEqual(response.status_code, 405)

    def test_delete_405(self):
        response = self.client.delete(
            self.ENDPOINT, self.seller_bills_examples[0]
        )
        self.assertEqual(response.status_code, 405)

    def test_wrong_format_500(self):
        response = self.client.post(
            self.ENDPOINT,
            [{"data_type": "Not a dict, that's for sure."}],
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 500)
