import argparse
import io
import json
import locale
import logging
import re
from datetime import datetime
from pathlib import Path

from django.core.management.base import BaseCommand, CommandError
from PyPDF2 import PdfReader, PageObject

logger = logging.getLogger(__name__)
locale.setlocale(locale.LC_TIME, "de_DE.UTF-8")


ADDRESSES_FIXED_DEFAULT = {
    "sender_address": {
        "gender": "m",
        "first_name": "Firstname 28",
        "last_name": "Lastname 28",
        "company_name": "SomeBigCompany GmbH",
        "street": "Streetname 27",
        "house_number": "123",
        "zip_code": "10123",
        "city": "Berlin",
        "country": "Deutschland",
        "iban": "DE12 1234 0000 1234 5678 90",
        "bic": "COBADEFFXXX",
        "tax_nr": "12/123/4567",
        "ust_id": "DE123456789",
        "hrb": "190099B",
        "bank": "Commerzbank",
    },
    "receiver_address": {
        "salutation": "Herr",
        "gender": "m",
        "first_name": "Firstname 29",
        "last_name": "Lastname 29",
        "street": "Streetname 28",
        "house_number": "4",
        "city": "Staffhorst",
        "country": "Deutschland",
        "zip_code": "27254",
        "tax_nr": "12/123/4567",
        "tax_id": "9876543210",
    },
}

DATE = datetime.now()
DATE_STR = f"Berlin, den {DATE.day}. {DATE.strftime('%B')} {DATE.year}"


class Command(BaseCommand):
    help = (
        "Reads bill data from PDFs and writes to JSON"
        "\n\nUsage example:"
        "\n\tpython manage.py parse_pdfs_dreifaltigkeit \\"
        "\n\t\t-p /path/to/rechnungen/ \\"
        "\n\t\t-a /path/to/addresses.json \\"
        "\n\t\t-j /path/to/write/data.json"
    )

    def create_parser(self, *args, **kwargs):
        parser = super(Command, self).create_parser(*args, **kwargs)
        parser.formatter_class = argparse.RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):
        parser.add_argument("-p", "--pdf_directory", nargs="?")
        parser.add_argument("-a", "--addresses_json", nargs="?", default=None)
        parser.add_argument(
            "-j",
            "--json_out",
            nargs="?",
            type=argparse.FileType("w+"),
        )

    def handle(self, *args, **options):
        pdf_directory: Path = Path(options["pdf_directory"])
        addresses_json_in: str = options["addresses_json"]
        if addresses_json_in is not None:
            with Path(options["addresses_json"]).open("r") as f:
                addresses = json.load(f)
                logger.info(
                    f"Using manual addresses: \n{json.dumps(addresses, indent=2)}"
                )
        else:
            addresses = ADDRESSES_FIXED_DEFAULT

        json_out: io.TextIOWrapper = options["json_out"]

        if not pdf_directory.is_dir():
            raise CommandError(f"{pdf_directory} is not a directory")

        data_json = []
        pdfs = [
            file for file in pdf_directory.iterdir() if file.suffix == ".pdf"
        ]
        for pdf_file in pdfs:
            logger.info(f"Parsing {pdf_file}")
            dreifalt = Dreifaltigkeit(pdf_file)
            dreifalt.compute_totals_overwrite_if_zero()

            data_json += (
                {
                    **dreifalt.rechnung.as_dict,
                    **addresses,
                    **{
                        "labels": {
                            "top_lines": [
                                f"Hiermit stornieren wir die Rechnung {dreifalt.rechnung.as_dict['bill_nr_pdf_reversed']} vom {dreifalt.rechnung.as_dict['bill_date_pdf_reversed'].replace('Berlin, den ', '')}."
                            ]
                        },
                        "doc_type": "",
                        "bill_date_pdf": DATE_STR,
                        "bill_nr": f"ST-{dreifalt.rechnung.as_dict['bill_nr_pdf_reversed']}",
                    },
                },
                {
                    **dreifalt.gutschrift.as_dict,
                    **addresses,
                    **{
                        "labels": {
                            "top_lines": [
                                f"Hiermit stornieren wir die Gutschrift {dreifalt.gutschrift.as_dict['bill_nr_pdf_reversed']} vom {dreifalt.gutschrift.as_dict['bill_date_pdf_reversed'].replace('Berlin, den ', '')}."
                            ]
                        },
                        "doc_type": "",
                        "bill_date_pdf": DATE_STR,
                        "bill_nr": f"ST-{dreifalt.gutschrift.as_dict['bill_nr_pdf_reversed']}",
                    },
                },
                {
                    **dreifalt.avis.as_dict,
                    **addresses,
                    **{
                        "labels": {
                            "top_lines": [
                                f"Hiermit stornieren wir das Zahlungsavis {dreifalt.avis.as_dict['bill_nr_pdf_reversed']} vom {dreifalt.avis.as_dict['bill_date_pdf_reversed'].replace('Berlin, den ', '')}."
                            ]
                        },
                        "doc_type": "avis",
                        "bill_date_pdf": DATE_STR,
                        "bill_nr": f"ST-{dreifalt.avis.as_dict['bill_nr_pdf_reversed']}",
                    },
                },
            )

        json.dump(data_json, json_out, indent=2)

        self.stdout.write(
            f"Parsing PDFs in {pdf_directory}; writing to {json_out}"
        )


def clean_lines(page: PageObject):
    return [line.strip() for line in page.extract_text().split("\n")]


class Dreifaltigkeit(object):
    def __init__(self, pdf_in):
        pdf_reader = PdfReader(pdf_in)
        self.rechnung = Rechnung(pdf_reader)
        self.gutschrift = Gutschrift(pdf_reader)
        self.avis = Avis(pdf_reader)

    def compute_totals_overwrite_if_zero(self):
        for i, part in enumerate([self.rechnung, self.gutschrift, self.avis]):
            if part.has_zero_totals:
                logger.warning(
                    f"Zero totals found in {part._data['bill_nr_pdf_reversed']}"
                )
                if i == 2:
                    rechnung_gross = make_comma_separated_string_numeric(
                        self.rechnung._data["charge_totals_reversed"][
                            "gross_value_string"
                        ]
                    )
                    gutschrift_gross = make_comma_separated_string_numeric(
                        self.gutschrift._data["charge_totals_reversed"][
                            "gross_value_string"
                        ]
                    )
                    # Special case for Avis
                    part._compute_totals(
                        rechnung_gross=rechnung_gross,
                        gutschrift_gross=gutschrift_gross,
                    )
                else:
                    part._compute_totals()


class DreifaltigkeitPart(object):
    def __init__(self, pdf_reader: PdfReader):
        self.lines_all: list[str] = self._filter_pages(pdf_reader)
        self._data: dict[str, str] = self._extract_info()

    @property
    def as_dict(self) -> dict:
        return self._data

    @property
    def has_zero_totals(self) -> bool:
        return any(
            [
                total in ["0,00", "-0,00"]
                for total in self._data["charge_totals_reversed"].values()
            ]
        )

    def _filter_pages(self, pdf_reader):
        lines_all = []
        in_doc_part = False
        for page in pdf_reader.pages:
            lines_page = clean_lines(page)
            words = [word for line in lines_page for word in line.split()]
            if self.string_stop in words:
                break
            elif self.string_start in words:
                in_doc_part = True
            if in_doc_part:
                lines_all += lines_page

        return lines_all

    def _extract_info(self) -> dict[str, str]:
        doc_nr_pdf = self._extract_by_replace(self.label_doc_nr)
        doc_date_pdf = self._extract_by_replace(self.label_doc_date_pdf)
        doc_period_pdf = self._extract_period()
        sum_net = self._extract_by_index(
            self.label_sum_net, len(self.label_sum_net.split())
        )

        if self.label_tax_rate:
            tax_rate_pdf = self._extract_by_index(self.label_tax_rate, 1)
            tax = self._extract_by_index(self.label_tax_rate, 2)
        else:
            tax_rate_pdf = None
            tax = None

        sum_gross = self._extract_by_index(self.label_sum_gross, 1)
        charges = self._extract_charges()

        return {
            "bill_nr_pdf_reversed": doc_nr_pdf,
            "bill_date_pdf_reversed": doc_date_pdf,
            "tax_rate_pdf": tax_rate_pdf,
            "bill_period": doc_period_pdf,
            "charge_totals_reversed": {
                "net_value_string": invert_sign(sum_net),
                "tax": invert_sign(tax),
                "gross_value_string": invert_sign(sum_gross),
            },
            "charges_reversed": [charge.as_dict for charge in charges],
        }

    def _get_line_hits(self, label: str) -> list[tuple[int, str]]:
        hits = [
            idx_line
            for idx_line in enumerate(self.lines_all)
            if label in idx_line[1]
        ]
        if len(hits) != 1:
            logger.warning(f"Label {label} returned {len(hits)} hits!")
        return hits

    def _get_lines_between(self, start: str, end: str) -> list[str]:
        if start in self.lines_all:
            idx_start = self.lines_all.index(start)
        else:
            idx_start = self._get_line_hits(start)[0][0]

        if end in self.lines_all:
            idx_end = self.lines_all.index(end)
        else:
            idx_end = self._get_line_hits(end)[0][0]

        return self.lines_all[idx_start : idx_end + 1]

    def _extract_by_replace(self, label: str) -> str:
        hits = self._get_line_hits(label)
        return hits[0][1].replace(label, "") if len(hits) > 0 else None

    def _extract_by_index(
        self, label: str, index: int, split_str: str | None = None
    ) -> str:
        hits = self._get_line_hits(label)
        return hits[0][1].split(split_str)[index] if len(hits) > 0 else None

    def _extract_charges(self) -> list["Charge"]:
        charges_lines = self._get_lines_between(
            self.string_pre_table, self.string_post_table
        )[1:-1]
        charges = []
        for idx, charge_line in enumerate(charges_lines):
            try:
                charge = Charge(charge_line)
            except IndexError:
                logger.warning(f"Empty charge line, parsing:\t{charge_line}")
                try:
                    charge = ChargeEmpty(charge_line, idx)
                except ValueError:
                    logger.warning(f"Invalid, skipping:\t{charge_line}")
                    charge = None

            if charge is not None:
                charges.append(charge)

        return charges

    def _extract_period(self):
        return self._extract_by_replace(self.label_period_pdf)

    def _compute_totals(self):
        for total in [
            make_comma_separated_string_numeric(total, none_okay=True)
            for total in self._data["charge_totals_reversed"].values()
        ]:
            if total != 0 and total is not None:
                logger.warning(
                    f"Non-zero total in {self._data['bill_nr_pdf_reversed']}: {total}"
                )

        total_net = 0
        for charge_dict in self._data["charges_reversed"]:
            total_net += make_comma_separated_string_numeric(
                charge_dict["net_value_string"]
            )

        tax = 0.19 * total_net
        total_gross = total_net + tax

        self._data["charge_totals_reversed"] = {
            "net_value_string": make_numeric_comma_separated_string(total_net),
            "tax": make_numeric_comma_separated_string(tax),
            "gross_value_string": make_numeric_comma_separated_string(
                total_gross
            ),
        }


class Rechnung(DreifaltigkeitPart):
    string_start = "Rechnung"
    string_stop = "Gutschrift"
    label_doc_nr = "Rechnungsnummer: "
    label_doc_date_pdf = "Rechnungsdatum: "
    label_period_pdf = "Leistungszeitraum: "
    label_sum_net = "Summe Nettobeträge"
    label_tax_rate = "Umsatzsteuer"
    label_sum_gross = "Rechnungsbetrag"
    string_pre_table = "EUR"
    string_post_table = "Summe Nettobeträge"


class Gutschrift(DreifaltigkeitPart):
    string_start = "Gutschrift"
    string_stop = "Zahlungsavis"
    label_doc_nr = "Rechnungsnummer: "
    label_doc_date_pdf = "Rechnungsdatum: "
    label_period_pdf = "Leistungszeitraum: "
    label_sum_net = "Summe Nettobeträge"
    label_tax_rate = "Umsatzsteuer"
    label_sum_gross = "Gutschriftsbetrag"
    string_pre_table = "EUR"
    string_post_table = "Summe Nettobeträge"


class Avis(DreifaltigkeitPart):
    string_start = "Zahlungsavis"
    string_stop = "erworbene"
    label_doc_nr = "Beleg: "
    label_doc_date_pdf = "Datum: "
    label_sum_net = "Gesamt"
    label_tax_rate = None
    label_sum_gross = "Zahlbetrag "
    string_pre_table = "EUR"
    string_post_table = "Gesamt"

    def _extract_charges(self):
        charges_lines = self._get_lines_between(
            self.string_pre_table, self.string_post_table
        )[1:-1]
        charges = []

        charges_lines = self._remove_known_invalid_lines(charges_lines)
        charges_lines = self._join_multiline_leistungen(charges_lines)

        for idx, charge_line in enumerate(charges_lines):
            try:
                charge = ChargeAvis(charge_line)
            except IndexError:
                logger.warning(f"Empty charge line, parsing:\t{charge_line}")
                try:
                    charge = ChargeAvisEmpty(charge_line, idx)
                except ValueError:
                    logger.warning(f"Invalid, skipping:\t{charge_line}")
            charges.append(charge)
        return charges

    def _remove_known_invalid_lines(self, charges_lines):
        known_invalid_lines = [
            "in EUR",
            "Leistung Rechnungsnummer DatumZahlbetrag",
        ]
        for line in known_invalid_lines:
            while True:
                try:
                    charges_lines.remove(line)
                except ValueError:
                    break

        return charges_lines

    def _join_multiline_leistungen(self, charges_lines_orig: list[str]):
        charges_lines = []
        carry = []

        for idx, charge_line in enumerate(charges_lines_orig):
            try:
                if not charge_line.endswith("€") and charges_lines_orig[
                    idx + 1
                ].endswith("€"):
                    carry = [charge_line]
                else:
                    charges_lines.append(" ".join(carry + [charge_line]))
                    carry = []
            except IndexError:
                charges_lines.append(charge_line)

        return charges_lines

    def _extract_period(self):
        pass

    def _compute_totals(self, rechnung_gross, gutschrift_gross):
        for total in [
            make_comma_separated_string_numeric(total, none_okay=True)
            for total in self._data["charge_totals_reversed"].values()
        ]:
            if total != 0 and total is not None:
                logger.warning(
                    f"Non-zero total in {self._data['bill_nr_pdf_reversed']}: {total}"
                )

        for label, total in zip(
            ["Storno Rechnung", "Storno Gutschrift"],
            [rechnung_gross, -1 * gutschrift_gross],
        ):
            idx, parsed_total = [
                (idx, di["net_value_string"])
                for (idx, di) in enumerate(self._data["charges_reversed"])
                if label in di["label"]
            ][0]
            if parsed_total in ["0,00", "-0,00"]:
                self._data["charges_reversed"][idx][
                    "net_value_string"
                ] = make_numeric_comma_separated_string(total)
            else:
                logger.warning(f"Avis {label} line not '0,00'")
                if not self._data["charges_reversed"][idx][
                    "net_value_string"
                ] == make_numeric_comma_separated_string(total):
                    logger.warning(
                        f"Avis label line not equal to computed total"
                    )

        total_net = 0
        for charge_dict in self._data["charges_reversed"]:
            total_net += make_comma_separated_string_numeric(
                charge_dict["net_value_string"]
            )

        # Different to Gutschrift/Rechnung
        tax = 0
        total_gross = total_net

        self._data["charge_totals_reversed"] = {
            "net_value_string": make_numeric_comma_separated_string(
                -1 * total_net
            ),
            "tax": make_numeric_comma_separated_string(-1 * tax),
            "gross_value_string": make_numeric_comma_separated_string(
                -1 * total_gross
            ),
        }


class Charge(object):
    def __init__(self, charge_str: str, idx: int | None = None):
        (
            self.description,
            self.number,
            self.price,
            self.sum,
        ) = self.parse_line(charge_str, idx)

    @property
    def as_dict(self):
        return {
            "label": self.description,
            "count": self.number,
            "value_per_unit": self.price,
            "net_value_string": self.sum,
        }

    def parse_line(self, charge_str: str, *_) -> tuple[str, float, float, float]:
        line_split = charge_str.split()
        return (
            "Storno " + " ".join(line_split[:-5]),
            line_split[-5],
            invert_sign(line_split[-4]),
            invert_sign(line_split[-2]),
        )


class ChargeAvis(Charge):
    def parse_line(self, charge_str: str, *_) -> tuple[str, float, float, float]:
        date_pattern = re.compile("\d{1,2}\. \w{3} 20\d{2}")
        match = re.search(date_pattern, charge_str)

        if match:
            date_str = charge_str[match.start() : match.end()]
            charge_parts_without_date = charge_str.replace(date_str, "").split()
            description = "Storno " + " ".join(charge_parts_without_date[:-3])
            doc_nr = charge_parts_without_date[-3]
            cost = charge_parts_without_date[-2]
        else:  # Date not included
            charge_parts = charge_str.split()
            description = "Storno " + " ".join(charge_parts[:-3])
            doc_nr = charge_parts[-3]
            date_str = ""
            cost = charge_parts[-2]

        return description, doc_nr, date_str, invert_sign(cost)

    @property
    def as_dict(self):
        return {
            "label": self.description,
            "count": "",
            "value_per_unit": "",
            "net_value_string": self.sum,
        }


class ChargeEmpty(Charge):
    def parse_line(self, charge_str: str, idx: int | None = None):
        value = charge_str.split()[1]
        make_comma_separated_string_numeric(value)

        return (f"Storno Pos. {idx + 1}", 1, value, value)


class ChargeAvisEmpty(Charge):
    def parse_line(self, charge_str: str, idx: int | None = None):
        value = charge_str.split()[0]
        make_comma_separated_string_numeric(value)

        return (f"Storno Pos. {idx + 1}", 1, value, value)


def invert_sign(value: str) -> str:
    if value is None:
        # Zahlungsavis includes no tax, so value is None.
        return None
    inverted = float(value.replace(",", ".")) * -1
    return f"{inverted:.2f}".replace(".", ",")


def make_comma_separated_string_numeric(
    num_str: str, none_okay: bool = False
) -> float:
    if none_okay and num_str is None:
        return None

    return float(num_str.replace(",", "."))


def make_numeric_comma_separated_string(num: float) -> str:
    return f"{num:.2f}".replace(".", ",")
