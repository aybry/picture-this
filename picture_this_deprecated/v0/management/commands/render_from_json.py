import argparse
import json
import logging
import requests

from django.urls import reverse
from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


APP_NAME = "v0"
PICTURE_THIS_URL_BASE = "http://picture-this:8000"


class Command(BaseCommand):
    help = (
        "Renders PDFs from JSON data"
        "\n\nUsage example:"
        "\n\tpython manage.py parse_pdfs_dreifaltigkeit \\"
        "\n\t\t-t reversal_bill \\"
        "\n\t\t-j /path/to/data.json"
        "\n\t\t-u http://localhost:8001"
    )

    def create_parser(self, *args, **kwargs):
        parser = super(Command, self).create_parser(*args, **kwargs)
        parser.formatter_class = argparse.RawTextHelpFormatter
        return parser

    def add_arguments(self, parser):
        parser.add_argument("-t", "--doc_type", nargs="?")
        parser.add_argument(
            "-j", "--json_in", nargs="?", type=argparse.FileType("r")
        )
        parser.add_argument(
            "-u", "--url_base", nargs="?", default=PICTURE_THIS_URL_BASE
        )

    def handle(self, *args, **options):
        doc_type = options["doc_type"]
        json_in = options["json_in"]
        url_base = options["url_base"].strip("/")

        data_json = json.load(json_in)

        url = url_base + reverse(f"{APP_NAME}:{doc_type}")
        for doc_data in data_json:
            logger.info(
                f"Rendering {doc_data['bill_nr']} ({doc_data['bill_nr_pdf_reversed']})"
            )
            response = requests.post(url, json=doc_data, timeout=10)
            logger.info(f"Rendered to {response.text}")
