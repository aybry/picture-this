import copy
from logging import getLogger
from pathlib import Path
from pprint import pprint

from django.conf import settings
from django.core.management import call_command
from rest_framework.request import Request
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import APIException

from .classes import PdfTemplate


logger = getLogger(__name__)


class ParserBaseView(APIView):
    """Endpoint for triggering parser.

    Required key:
        - `pdf_directory`: path (within PT container!) to directory of
            Holy Trinity PDFs

    Optional keys:
        - `json_out`: Path to write parsed data to
          defaults to: `pdf_directory / "parsed_data.json"`
        - `addresses_json`: Path pointing to manual address entry
          defaults to: `None` (default addresses are hard-coded)

    Returns:
        - 200: all's good (with hint on where to find the JSON)
        - 411: your JSON data is invalid
        - 500: something went wrong (with hint to look at PT logs)
    """

    def get(self, _):
        return Response("Please POST me your data...")

    def post(self, request: Request):
        """Default post method for simple, single PDF template."""
        data: dict = request.data

        if data.get("json_out") is not None:
            json_out = Path(data["json_out"])
        else:
            json_out = Path(data["pdf_directory"]) / "parsed_data.json"

        try:
            with json_out.open("w+") as json_out_f:
                call_command(
                    "parse_pdfs_dreifaltigkeit",
                    pdf_directory=data["pdf_directory"],
                    addresses_json=data.get("addresses_json"),
                    json_out=json_out_f,
                )
        except:
            logger.exception("O noes")
            return Response(
                "Something went wrong. D'oh! Check picture-this logs or ask Sam. His hourly rate to fix it is more than fair 👍😉",
                status=500,
            )

        return Response(f"Wrote to {json_out}")


class RendererBaseView(APIView):
    def get(self, _):
        return Response("Please POST me your data...")

    def post(self, request: Request):
        """Default post method for simple, single PDF template."""
        data: dict = request.data

        if not isinstance(data, dict):
            raise APIException("Data is not a dict!")

        if data.get("labels") is None:
            data["labels"] = {}

        logger.info(f"Bill no.: {data['bill_nr']}")
        logger.debug(data)

        pdf_template = PdfTemplate(self.TEMPLATE_IDENTIFIER, data)
        pdf_output_path = pdf_template.write()

        return self.respond_relative_path(pdf_output_path)

    def respond_relative_path(self, absolute_path: Path) -> Path:
        return Response(str(absolute_path.relative_to(settings.PT_DATA_DIR)))


class AgencyBillView(RendererBaseView):
    TEMPLATE_IDENTIFIER = "agency_bill"


class ReversalBillView(RendererBaseView):
    TEMPLATE_IDENTIFIER = "reversal_bill"


class SellerBillView(RendererBaseView):
    TEMPLATE_IDENTIFIER = "seller_bill"

    BILL_CONFIG = {
        "rechnung": {
            "type": "rechnung",
            "title_default": "Rechnung",
            "title_key": "rechnung_title",
            "document_nr_label_default": "Rechnungsnummer",
            "document_nr_label_key": "rechnung_document_nr",
            "document_nr_data_key": "charges_bill_nr",
            "recipient_header_key": "rechnung_recipient_header",
            "recipient_header_default": "Leistungsempfänger",
            "data_key": "charges",
            "totals_key": "charge_totals",
            "columns": [
                {
                    "label_key": "rechnung_label_col",
                    "label_default": "Leistung",
                    "key": "label",
                    "classes": "widest",
                },
                {
                    "label_key": "rechnung_count_col",
                    "label_default": "Anzahl",
                    "key": "count",
                    "classes": "numeric",
                },
                {
                    "label_key": "rechnung_value_per_unit_col",
                    "label_default": "Preis in EUR",
                    "key": "value_per_unit",
                    "classes": "currency",
                },
                {
                    "label_key": "rechnung_net_value_string_col",
                    "label_default": "Summe in EUR",
                    "key": "net_value_string",
                    "classes": "currency",
                },
            ],
            "totals_rows": [
                {
                    "label_key": "rechnung_net_value_sum",
                    "label_default": "Summe Nettobeträge",
                    "key": "net_value_string",
                },
                {
                    "label": "{} {}",
                    "label_config": [
                        # (data_key, default)
                        ("labels.rechnung_tax", "Umsatzsteuer"),
                        ("tax_rate_pdf", ""),
                    ],
                    "key": "tax",
                },
                {
                    "label_key": "rechnung_gross_value_sum",
                    "label_default": "Rechnungsbetrag",
                    "key": "gross_value_string",
                },
            ],
        },
        "gutschrift": {
            "type": "gutschrift",
            "title_default": "Gutschrift",
            "title_key": "gutschrift_title",
            "document_nr_label_default": "Rechnungsnummer",
            "document_nr_label_key": "gutschrift_document_nr",
            "document_nr_data_key": "credits_bill_nr",
            "recipient_header_key": "gutschrift_recipient_header",
            "recipient_header_default": "Leistender Unternehmer",
            "data_key": "credits",
            "totals_key": "credit_totals",
            "columns": [
                {
                    "label_key": "gutschrift_label_col",
                    "label_default": "Leistung",
                    "key": "label",
                    "classes": "widest",
                },
                {
                    "label_key": "gutschrift_count_col",
                    "label_default": "Anzahl",
                    "key": "count",
                    "classes": "numeric",
                },
                {
                    "label_key": "gutschrift_value_per_unit_col",
                    "label_default": "Preis in EUR",
                    "key": "value_per_unit",
                    "classes": "currency",
                },
                {
                    "label_key": "gutschrift_net_value_string_col",
                    "label_default": "Summe in EUR",
                    "key": "net_value_string",
                    "classes": "currency",
                },
            ],
            "totals_rows": [
                {
                    "label_key": "gutschrift_net_value_sum",
                    "label_default": "Summe Nettobeträge",
                    "key": "net_value_string",
                },
                {
                    "label": "{} {}",
                    "label_config": [
                        # (data_key, default)
                        ("labels.gutschrift_tax", "Umsatzsteuer"),
                        ("credit_tax_rate_pdf", ""),
                    ],
                    "key": "tax",
                },
                {
                    "label_key": "gutschrift_gross_value_sum",
                    "label_default": "Gutschriftsbetrag",
                    "key": "gross_value_string",
                },
            ],
        },
        "zahlungsavis": {
            "type": "zahlungsavis",
            "title_default": "Zahlungsavis",
            "title_key": "zahlungsavis_title",
            "document_nr_label_default": "Beleg",
            "document_nr_label_key": "zahlungsavis_document_nr",
            "document_nr_data_key": "avis_bill_nr",
            "recipient_header_key": "zahlungsavis_recipient_header",
            "recipient_header_default": "Leistender Unternehmer",
            "data_key": "avis",
            "totals_key": "avis_totals",
            "columns": [
                {
                    "label_key": "zahlungsavis_label_col",
                    "label_default": "Leistung",
                    "key": "label",
                    "classes": "widest",
                },
                {
                    "label_key": "zahlungsavis_bill_nr_col",
                    "label_default": "Rechnungsnummer",
                    "key": "bill_nr",
                },
                {
                    "label_key": "zahlungsavis_date_col",
                    "label_default": "Datum",
                    "key": "date",
                },
                {
                    "label_key": "zahlungsavis_gross_value_string_col",
                    "label_default": "Zahlbetrag in EUR",
                    "key": "gross_value_string",
                    "classes": "currency",
                },
            ],
            "totals_rows": [
                {
                    "label_key": "zahlungsavis_total_string",
                    "label_default": "Gesamt",
                    "key": "total_string",
                },
                {
                    "label_key": "zahlungsavis_payable_string",
                    "label_default": "Zahlbetrag",
                    "key": "payable_string",
                },
            ],
        },
    }

    SUMMARY_CONFIG = {
        "leaderwerb": {
            "header_key": "lead_list_title",
            "header_default": "Erworbene Leads",
            "data_key": "lead_cost_list",
            "columns": [
                {
                    "label_key": "lead_list_col1",
                    "label_default": "Pos.",
                    "type": "count",
                },
                {
                    "label_key": "lead_list_col2",
                    "label_default": "Lead",
                    "key": "label",
                    "classes": "widest",
                },
                {
                    "label_key": "lead_list_col3",
                    "label_default": "Erstellt",
                    "key": "created_at",
                },
                {
                    "label_key": "lead_list_col4",
                    "label_default": "Betrag",
                    "classes": "currency",
                    "key": "net_value_string",
                },
            ],
        },
        "reklamierte_leads": {
            "header_key": "lead_reclaim_title",
            "header_default": "Reklamierte Leads",
            "data_key": "lead_reclaim_credit_list",
            "columns": [
                {
                    "label_key": "lead_reclaim_col1",
                    "label_default": "Pos.",
                    "type": "count",
                },
                {
                    "label_key": "lead_reclaim_col2",
                    "label_default": "Lead",
                    "key": "label",
                    "classes": "widest",
                },
                {
                    "label_key": "lead_reclaim_col3",
                    "label_default": "Erstellt",
                    "key": "created_at",
                },
                {
                    "label_key": "lead_reclaim_col4",
                    "label_default": "Betrag",
                    "classes": "currency",
                    "key": "net_value_string",
                },
            ],
        },
        "provision": {
            "header_key": "provision_table_title",
            "header_default": "Provision",
            "data_key": "provision_entries",
            "columns": [
                {
                    "label_key": "provisions_col1",
                    "label_default": "Pos.",
                    "type": "count",
                },
                {
                    "label_key": "provisions_col2",
                    "label_default": "Lead",
                    "key": "label",
                    "classes": "widest",
                },
                {
                    "label_key": "provisions_col3",
                    "label_default": "M-Prov.",
                    "classes": "currency",
                    "key": "full_value",
                },
                {
                    "label_key": "provisions_col4",
                    "label_default": "Start",
                    "key": "start",
                },
                {
                    "label_key": "provisions_col5",
                    "label_default": "Ende",
                    "key": "end",
                },
                {
                    "label_key": "provisions_col6",
                    "label_default": "Tage",
                    "key": "duration",
                },
                {
                    "label_key": "provisions_col7",
                    "label_default": "Provision",
                    "classes": "currency",
                    "key": "net_value_string",
                },
            ],
        },
    }

    def post(self, request: Request):
        data: dict = request.data

        if not isinstance(data, dict):
            raise APIException("Data is not a dict!")

        if data.get("labels") is None:
            data["labels"] = {}

        logger.info(f"Bill no.: {data['bill_nr']}")

        all_pages = []

        # Invoice section
        for label, _config in self.BILL_CONFIG.items():
            logger.info(f"Processing {label}...")

            config = copy.deepcopy(_config)
            logger.info(pprint(config))

            for idx, row_config in enumerate(config["totals_rows"]):
                if row_config.get("label_config"):
                    labels = []
                    for key, default in row_config["label_config"]:
                        if "labels." in key:
                            labels.append(
                                data["labels"].get(key.split(".")[1], default)
                            )
                        else:
                            labels.append(data.get(key, default))

                    config["totals_rows"][idx] = {
                        "label_default": row_config["label"].format(*labels),
                        "key": row_config["key"],
                    }

            if config["data_key"] in data and len(data[config["data_key"]]) > 0:
                pdf_template = PdfTemplate(
                    self.TEMPLATE_IDENTIFIER, {**data, **config}
                )
                rendered = pdf_template.html.render(
                    stylesheets=pdf_template.css,
                    font_config=pdf_template.font_config,
                )
                all_pages += [page for page in rendered.pages]
            else:
                logger.info(f"No data found.")

        # Item list section
        for label, config in self.SUMMARY_CONFIG.items():
            logger.info(f"Processing {label}...")

            if config["data_key"] in data and len(data[config["data_key"]]) > 0:
                pdf_template = PdfTemplate(
                    f"{self.TEMPLATE_IDENTIFIER}__list", {**data, **config}
                )
                rendered = pdf_template.html.render(
                    stylesheets=pdf_template.css,
                    font_config=pdf_template.font_config,
                )
                all_pages += [page for page in rendered.pages]
            else:
                logger.info(f"No data found.")

        pdf_output_path = pdf_template.get_output_path()
        rendered.copy(all_pages).write_pdf(pdf_output_path)

        return self.respond_relative_path(pdf_output_path)
