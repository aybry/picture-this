from django.urls import path

from . import views


app_name = "v0"

urlpatterns = [
    path("agency_bill", views.AgencyBillView.as_view(), name="agency_bill"),
    path("seller_bill", views.SellerBillView.as_view(), name="seller_bill"),
    path(
        "reversal_bill", views.ReversalBillView.as_view(), name="reversal_bill"
    ),
    path("parse_pdfs", views.ParserBaseView.as_view(), name="parse_pdfs"),
]
