import logging

from django.conf import settings
from django.core.management.base import BaseCommand
from v1.components import MAPPER


logger = logging.getLogger(__name__)


APP_NAME = "v1"
DOC_OUTPUT_FILE = settings.V1_BASE_DIR / "docs" / "components.md"
ACTIVE_COMPONENTS = [component for _, component in MAPPER.items()]


class Command(BaseCommand):
    help = "Creates documentation for v1 components"

    def handle(self, *args, **options):
        docs_content = (
            "<!-- This document is autogenerated! Do not edit manually! -->\n\n"
        )

        for component_class in ACTIVE_COMPONENTS:
            docs_content += component_class.render_docs()
            docs_content += "\n<!-- This document is autogenerated! Do not edit manually! -->\n\n"

        with DOC_OUTPUT_FILE.open("w+") as f:
            f.write(docs_content)
