from datetime import datetime
from pathlib import Path

from django.http import HttpResponse
from loguru import logger
from rest_framework import status
from rest_framework.exceptions import APIException, ParseError
from rest_framework.parsers import FileUploadParser
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet

from v1.components.document import Document
from v1.models import UploadedImage
from v1.serializers import ImageUploadSerializer


class RenderView(APIView):
    def get(self, _):
        return Response("Please POST me your data...")

    def post(self, request: Request):
        data: dict = request.data

        try:
            document = Document(data)
        except Exception as exc:
            logger.exception(exc)
            raise APIException("Something went wrong. Check logs.")

        output_path = document.render()

        if document.parsed_data["return_as"] == "path":
            response_content = {"output_path": str(output_path)}
            return Response(response_content, status=status.HTTP_200_OK)
        elif document.parsed_data["return_as"] == "pdf":
            with output_path.open("rb") as f:
                output_pdf = f.read()
                return HttpResponse(output_pdf, content_type="application/pdf")
        else:
            raise APIException(
                "Something went wrong. Errors not yet properly logged..."
            )


class ImageUploadViewSet(ViewSet):
    serializer_class = ImageUploadSerializer

    def list(self, request):
        return Response("Upload your image")

    def create(self, request):
        if "image" not in request.FILES:
            raise APIException("Empty content")

        upload = request.FILES["image"]
        uploaded_image = UploadedImage.objects.create()
        image_filename = f"{datetime.now().isoformat()}_{str(uploaded_image.id)}{Path(upload.name).suffix}"
        uploaded_image.image.save(image_filename, upload, save=True)

        response_content = {"image_id": uploaded_image.pk}
        return Response(response_content, status=status.HTTP_201_CREATED)
