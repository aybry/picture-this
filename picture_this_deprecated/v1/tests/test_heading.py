from django.test import TestCase

from v1.tests._tester import TesterBase
from v1.components.heading import Heading


VERSION = "v1"


class HeadingTestCase(TesterBase, TestCase):
    FIXTURE_LABEL = Heading.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(Heading)

    def test_invalid_tag(self):
        erroneous = {"content": "This is fine", "tag": "This is not"}
        with self.assertRaises(ValueError):
            Heading(erroneous)
