from django.test import TestCase

from v1.tests._tester import TesterBase
from v1.components.doc_info import DocInfo


VERSION = "v1"


class DocInfoTestCase(TesterBase, TestCase):
    FIXTURE_LABEL = DocInfo.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(DocInfo)
