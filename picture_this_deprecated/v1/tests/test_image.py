from django.test import TestCase

from v1.tests._tester import TesterBase
from v1.components.image import Image
from v1.models import UploadedImage


VERSION = "v1"


class ImageTestCase(TesterBase, TestCase):
    FIXTURE_LABEL = Image.COMPONENT_LABEL

    def setUp(self):
        UploadedImage.objects.create(
            pk="82366cce-bcd5-4c82-bd67-aaa3b0a69828",
            image="v1/tests/fixtures/_img/favicon.ico",
        )

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(Image)
