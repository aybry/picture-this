from django.test import TestCase

from v1.tests._tester import TesterBase
from v1.components.address import Address


VERSION = "v1"


class AddressTestCase(TesterBase, TestCase):
    FIXTURE_LABEL = Address.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(Address)
