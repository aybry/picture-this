from django.test import TestCase

from v1.tests._tester import TesterBase
from v1.components.block_text import BlockText


VERSION = "v1"


class BlockTextTestCase(TesterBase, TestCase):
    FIXTURE_LABEL = BlockText.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(BlockText)
