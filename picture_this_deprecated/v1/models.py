import uuid

from django.db import models


class UploadedImage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    uploaded_at = models.DateTimeField(auto_now_add=True, editable=False)
    image = models.ImageField(blank=False, null=False, upload_to="uploads/")
    size = models.PositiveBigIntegerField(null=True, editable=False)
