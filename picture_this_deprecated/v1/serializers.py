from rest_framework.serializers import Serializer, ImageField


class ImageUploadSerializer(Serializer):
    image = ImageField()

    class Meta:
        fields = ["image"]
