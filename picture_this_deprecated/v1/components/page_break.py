import logging

from v1.components.base import Component, Option


logger = logging.getLogger(__name__)


class PageBreak(Component):
    COMPONENT_LABEL = "page_break"
    DESCRIPTION = """Forces next component onto new page."""
