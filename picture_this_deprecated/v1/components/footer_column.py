import logging

from v1.components.base import Component, Option

logger = logging.getLogger(__name__)


class FooterColumn(Component):
    COMPONENT_LABEL = "footer_column"
    DESCRIPTION = "Single column in footer. Not intended for direct assignment; see `Footer`."

    lines = Option(
        label="lines",
        help_text="Footer column lines.",
        type=list[str],
    )

    table = Option(
        label="table",
        help_text="Footer table defined by array of arrays of strings.",
        type=list[list[str]],
    )

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "",
            "middle": "align-middle",
            "right": "align-right",
        },
    )

    justify_text = Option(
        label="justify_text",
        help_text="Sets text justification for entire column.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        choice_mapper={
            "left": "",
            "middle": "justify-middle",
            "right": "justify-right",
        },
    )
