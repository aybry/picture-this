import logging

from v1.components.base import Component, Option
from v1.components.footer_column import FooterColumn

logger = logging.getLogger(__name__)


class Footer(Component):
    COMPONENT_LABEL = "footer"
    DESCRIPTION = "Page footer. Fixed on every page to the bottom. Defined by an array of FooterColumns. TODO: Optional Page number."
    # TODO: Optional Page number.

    columns = Option(
        label="columns",
        help_text="Defines columns in footer. See component `FooterColumn` for options.",
        type=list[FooterColumn],
    )

    def parse_data(self) -> dict:
        raw_data = self._raw_data.copy()

        columns = []
        for footer_column_dict in raw_data.pop("columns", []):
            columns.append(FooterColumn(footer_column_dict))

        parsed_data = super().parse_data(raw_data)
        parsed_data.update({"columns": columns})

        return parsed_data
