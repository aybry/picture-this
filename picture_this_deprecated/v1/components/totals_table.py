import logging

from v1.components.base import Component, Option

logger = logging.getLogger(__name__)


class TotalsTable(Component):
    COMPONENT_LABEL = "totals_table"
    DESCRIPTION = "Totals (summary) lines for data table with bold, top/bottom bordered final line. A convenience component, since Table can render the same."

    rows = Option(
        label="rows",
        help_text="",
        type=list[list[str]],
    )

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="right",
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "align-left",
            "middle": "",
            "right": "align-right",
        },
    )
