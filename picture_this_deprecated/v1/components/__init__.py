from v1.components.image import Image
from v1.components.address import Address
from v1.components.heading import Heading
from v1.components.doc_info import DocInfo
from v1.components.block_text import BlockText
from v1.components.table import Table
from v1.components.column_header import ColumnHeader
from v1.components.page_break import PageBreak
from v1.components.totals_table import TotalsTable
from v1.components.footer import Footer
from v1.components.footer_column import FooterColumn


MAPPER = {
    Image.COMPONENT_LABEL: Image,
    Address.COMPONENT_LABEL: Address,
    Heading.COMPONENT_LABEL: Heading,
    DocInfo.COMPONENT_LABEL: DocInfo,
    BlockText.COMPONENT_LABEL: BlockText,
    Table.COMPONENT_LABEL: Table,
    ColumnHeader.COMPONENT_LABEL: ColumnHeader,
    TotalsTable.COMPONENT_LABEL: TotalsTable,
    PageBreak.COMPONENT_LABEL: PageBreak,
    Footer.COMPONENT_LABEL: Footer,
    FooterColumn.COMPONENT_LABEL: FooterColumn,
}
