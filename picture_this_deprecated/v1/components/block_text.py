import logging

from v1.components.base import Component, Option

logger = logging.getLogger(__name__)


class BlockText(Component):
    COMPONENT_LABEL = "block_text"
    DESCRIPTION = """A block of text."""

    content = Option(
        label="content",
        help_text="Content of text block",
        type=str,
    )
