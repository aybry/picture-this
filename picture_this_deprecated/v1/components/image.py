from django.shortcuts import get_object_or_404

from v1.components.base import Component, Option
from v1.models import UploadedImage


class Image(Component):
    COMPONENT_LABEL = "image"
    DESCRIPTION = """Image component. Works with images and corresponding UUIDs uploaded via v1/upload."""

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "",
            "middle": "align-middle",
            "right": "align-right",
        },
    )

    uuid = Option(
        label="uuid",
        help_text="UUID of image (supplied on file upload)",
        type=str,
    )

    def parse_data(self) -> dict:
        raw_data = self._raw_data.copy()
        parsed_data = super().parse_data()

        image_uuid = raw_data.pop("uuid")
        uploaded_image = get_object_or_404(UploadedImage, pk=image_uuid)

        parsed_data.update({"image_url": uploaded_image.image.url})

        return parsed_data
