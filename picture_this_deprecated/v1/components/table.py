import logging

from v1.components.base import Component, Option
from v1.components.column_header import ColumnHeader

logger = logging.getLogger(__name__)


class Table(Component):
    COMPONENT_LABEL = "table"
    DESCRIPTION = "Data table. Use for listing positions, respective costs, etc."

    column_headers = Option(
        label="column_headers",
        help_text="Table column headers. Also used for column-wide options. See component `ColumnHeader` for options",
        type=list[ColumnHeader],
    )

    show_column_header_content = Option(
        label="show_column_header_content",
        help_text="Show the content of column headers in <thead>. If false, other ColumnHeader options are still applied.",
        default=True,
        type=bool,
    )

    table_type = Option(
        label="table_type",
        # TODO: proper help text
        help_text="Determines which styling presets to use for table",
        type=str,
        default="data_table",
        choices=("data_table", "totals_table", "summary_table"),
        choice_mapper={
            "data_table": "data-table",
            "totals_table": "totals-table",
            "summary_table": "summary-table",
        },
    )

    rows = Option(
        label="rows",
        help_text="Table rows. Number of cells per row must equal number of `column_headers`.",
        type=list[list[str]],
    )

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="middle",
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "align-left",
            "middle": "",
            "right": "align-right",
        },
    )

    justify_text = Option(
        label="justify_text",
        help_text="Sets text justification for entire column.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        choice_mapper={
            "left": "",
            "middle": "justify-middle",
            "right": "justify-right",
        },
    )

    fill_width = Option(
        label="fill_width",
        help_text="Set table width to page width.",
        default=True,
        type=bool,
        choices=(True, False),
        choice_mapper={True: "", False: "no-fill-width"},
    )

    numbered_rows = Option(
        label="numbered_rows",
        help_text="Add extra column with row numbers.",
        default=False,
        type=bool,
    )

    numbered_row_label = Option(
        label="numbered_row_label",
        help_text="Custom header for column containing row numbers.",
        default="Pos.",
        type=str,
    )

    numbered_row_template = Option(
        label="numbered_row_template",
        help_text="Template for row numbers with `{idx}` denoting the row number, e.g. `'Row no. {idx}'`",
        default="{idx}.",
        type=str,
    )

    colour_alternate_rows = Option(
        label="colour_alternate_rows",
        help_text="Colour the background of every second row (light grey)",
        default=True,
        type=bool,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        table_functions = {
            "zip": zip,
            "row_contains_rowspan": row_contains_rowspan,
            "rowspan_length": rowspan_length,
        }
        self._env.globals.update(**table_functions)

    def parse_data(self) -> dict:
        raw_data = self._raw_data.copy()

        column_headers = []
        for column_header_dict in raw_data.pop("column_headers", []):
            column_headers.append(ColumnHeader(column_header_dict))

        parsed_data = super().parse_data(raw_data)
        parsed_data.update({"column_headers": column_headers})

        return parsed_data


def row_contains_rowspan(row):
    for cell in row:
        if isinstance(cell, list):
            return True
    return False


def rowspan_length(row):
    for cell in row:
        if isinstance(cell, list):
            return len(cell)
