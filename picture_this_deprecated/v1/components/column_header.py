import logging

from v1.components.base import Component, Option

logger = logging.getLogger(__name__)


class ColumnHeader(Component):
    COMPONENT_LABEL = "column_header"
    DESCRIPTION = "Column header cell for table. Not intended for direct assignment; see `Table`."

    content = Option(
        label="content",
        help_text="The content of the header cell",
        default="",
        type=str,
    )

    justify_text = Option(
        label="justify_text",
        help_text="Sets text justification for entire column.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        choice_mapper={
            "left": "",
            "middle": "justify-middle",
            "right": "justify-right",
        },
    )

    data_type = Option(
        label="data_type",
        help_text="Data type of column.",
        default="text",
        type=str,
        choices=("text", "numeric", "currency"),
        jinja_variable="type_class",
        choice_mapper={
            "text": "",
            "numeric": "numeric",
            "currency": "currency",
        },
    )

    fill_width = Option(
        label="fill_width",
        help_text="Grow column to fill table's remaining width.",
        default=False,
        type=bool,
        choices=(True, False),
        choice_mapper={True: "wide", False: ""},
    )

    rowspan = Option(
        label="rowspan",
        help_text="Whether this column is expected to contain data spanning multiple rows. If true, table parser expects a list of cell data. Note: This option is only aligned well if there is no line break in any of the content.",
        default=False,
        type=bool,
        choices=(True, False),
        choice_mapper={True: "custom-rowspan", False: ""},
    )
