from django.urls import path

from . import views


app_name = "v1"

urlpatterns = [
    path("render", views.RenderView.as_view(), name="render"),
    path(
        "upload",
        views.ImageUploadViewSet.as_view({"get": "list", "post": "create"}),
        name="upload",
    ),
]
