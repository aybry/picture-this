from loguru import logger
from pathlib import Path


log_file_path = Path("/") / "logs" / "picture-this.log"

logger.add(log_file_path, rotation="100 MB")
