from django.urls import path, include, re_path
from django.views.static import serve
from django.conf import settings

from project.healthcheck import are_you_healthy


urlpatterns = [
    path("are_you_healthy", are_you_healthy, name="healthcheck"),
    path("v0/", include("v0.urls")),
    path("v1/", include("v1.urls")),
    re_path(
        r"^media/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT}
    ),
]
