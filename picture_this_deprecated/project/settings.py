import os
import ast
from pathlib import Path


DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))
LOG_LEVEL = os.getenv("DEBUG_LEVEL", "DEBUG")
ENVIRONMENT = os.getenv("ENVIRONMENT", "local")

BASE_DIR = Path(__file__).resolve().parent.parent

if ENVIRONMENT == "local_nodocker":
    PT_DATA_DIR = BASE_DIR / "pt_data"
else:
    PT_DATA_DIR = Path("/") / "pt_data"

PDF_OUTPUT_DIR = PT_DATA_DIR / "pdf"
LOGS_DIR = PT_DATA_DIR / "logs"

for pt_dir in [
    PT_DATA_DIR,
    PDF_OUTPUT_DIR,
    LOGS_DIR,
]:
    try:
        pt_dir.mkdir(exist_ok=True, parents=True)
    except PermissionError:
        print(f"Could not create {pt_dir}. Ignoring.")


SECRET_KEY_ENV = os.getenv("DJANGO_SECRET_KEY")
SECRET_KEY = SECRET_KEY_ENV if SECRET_KEY_ENV != "" else "local-secret"  # nosec


ENV_ALLOWED_HOSTS = os.getenv("ALLOWED_HOSTS", "").split(",")

ALLOWED_HOSTS = [
    "picture-this",
    "localhost",
] + ENV_ALLOWED_HOSTS


INSTALLED_APPS = [
    "v1",
    "v0",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
]

if DEBUG:
    INSTALLED_APPS.insert(0, "whitenoise.runserver_nostatic")

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

V1_BASE_DIR = BASE_DIR / "v1"
STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR.parent / "srv" / "staticfiles"
STATICFILES_DIRS = [
    BASE_DIR / "project" / "static",
    V1_BASE_DIR / "static",
]
V1_STYLESHEET = STATIC_ROOT / "v1" / "css" / "style.css"
MEDIA_ROOT = "/media/"
MEDIA_URL = "media/"

WSGI_APPLICATION = "project.wsgi.application"

TIME_ZONE = "UTC"
USE_TZ = True

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("DATABASE_NAME"),
        "USER": os.getenv("DATABASE_USER"),
        "PASSWORD": os.getenv("DATABASE_PASSWORD"),
        "HOST": os.getenv("DATABASE_HOST"),
        "PORT": os.getenv("DATABASE_PORT"),
        "TEST": {"NAME": f"test_{os.getenv('DATABASE_NAME')}"},
    }
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{asctime} - {name} - {levelname} - {message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "file": {
            "level": "WARNING",
            "class": "logging.FileHandler",
            "filename": str(LOGS_DIR / "warnings.log"),
            "formatter": "verbose",
        },
    },
    "loggers": {
        "v0": {
            "handlers": ["console", "file"],
            "propagate": True,
            "level": LOG_LEVEL,
        },
        "v1": {
            "handlers": ["console", "file"],
            "propagate": True,
            "level": LOG_LEVEL,
        },
    },
}
