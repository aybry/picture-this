lint:
	docker-compose exec picture-this bandit . -r -x "*_test.py"

test:
	docker-compose exec picture-this python -m pytest .

docs:
	docker-compose exec picture-this python cli.py create-docs