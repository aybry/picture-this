from unittest import TestCase, skip

from renderer.tests._tester import TesterBase
from renderer.components.image import ImageComponent

from renderer.models.children.image import (
    validate_guid_is_valid,
    validate_image_exists,
    validate_image_input,
)


class ImageTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = ImageComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(ImageComponent)

    def test_validator_fails_on_non_uuid(self):
        invalid_guid = "not-a-valid-uuid"
        with self.assertRaises(ValueError):
            validate_guid_is_valid(invalid_guid)
        with self.assertRaises(ValueError):
            validate_image_input(invalid_guid)

    def test_validator_fails_on_file_not_found(self):
        valid_guid_of_nonextistent_file = "cbf953fd-ddff-4f7e-9cf7-7e3edf45b50d"
        with self.assertRaises(ValueError):
            validate_image_exists(valid_guid_of_nonextistent_file)
        with self.assertRaises(ValueError):
            validate_image_input(valid_guid_of_nonextistent_file)
