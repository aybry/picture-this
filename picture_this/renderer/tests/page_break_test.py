from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.page_break import PageBreakComponent


class PageBreakTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = PageBreakComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(PageBreakComponent)
