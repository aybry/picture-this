from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.footer import FooterComponent


class FooterTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = FooterComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(FooterComponent)
