from unittest import TestCase, skip

from renderer.tests._tester import TesterBase
from renderer.components.document import DocumentComponent

# from renderer.models import UploadedImage


# @skip("UploadedImage not yet migrated")
class DocumentTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = DocumentComponent.COMPONENT_LABEL

    # def setUp(self):
    #     UploadedImage.objects.create(
    #         pk="82366cce-bcd5-4c82-bd67-aaa3b0a69828",
    #         image="v1/tests/fixtures/_img/favicon.ico",
    #     )

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    # def test_fixtures(self):
    #     super()._test_fixtures(DocumentComponent)
