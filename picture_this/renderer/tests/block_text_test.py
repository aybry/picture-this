from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.block_text import BlockTextComponent


class BlockTextTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = BlockTextComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(BlockTextComponent)
