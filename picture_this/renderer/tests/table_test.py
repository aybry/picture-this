from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.table import TableComponent


class TableTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = TableComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(TableComponent)
