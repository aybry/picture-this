from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.heading import HeadingComponent


class HeadingTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = HeadingComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(HeadingComponent)

    def test_invalid_tag(self):
        erroneous = {"content": "This is fine", "tag": "This is not"}
        with self.assertRaises(ValueError):
            HeadingComponent(erroneous)
