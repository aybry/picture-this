import json
from pathlib import Path

from bs4 import BeautifulSoup
from settings import settings

from api.helpers.render_helpers import create_document_dict_from_component_dict
from renderer.components import AnyComponentType
from renderer.components.base import Component
from renderer.components.document import DocumentComponent


TESTS_DIR = settings.BASE_DIR / "renderer" / "tests"


class TesterBase(object):
    maxDiff = None

    @classmethod
    def _get_input_output_fixtures(
        cls, component_label: str
    ) -> list[tuple[dict, str]]:
        fixtures = []

        fixtures_toplevel_dir: Path = TESTS_DIR / "fixtures" / component_label
        fixture_dirs: list[Path] = sorted(
            [
                dir_path
                for dir_path in fixtures_toplevel_dir.iterdir()
                if not str(dir_path.name).startswith("_")
            ],
            key=lambda path: str(path.name),
        )

        for dir_path in fixture_dirs:
            input_filepath: Path = dir_path / "input.json"
            output_filepath: Path = dir_path / "output.html"

            with input_filepath.open() as f_in, output_filepath.open() as f_out:
                fixtures.append((dir_path.name, json.load(f_in), f_out.read()))

        return fixtures

    @property
    def fixtures(self):
        return self._get_input_output_fixtures(self.COMPONENT_LABEL)

    def _render_fixtures(self):
        regression_test_output_dir = (
            TESTS_DIR / "regression_output" / self.COMPONENT_LABEL
        )
        regression_test_output_dir.mkdir(exist_ok=True, parents=True)

        for fixture_label, json_in, _ in self.fixtures:
            data = create_document_dict_from_component_dict(json_in)
            document = DocumentComponent(data)
            document.render(
                output_dir=regression_test_output_dir,
                file_basename=fixture_label,
            )

    def _test_fixtures(self, component_class: Component, skip_bs4: bool = False):
        self._render_fixtures()
        for fixture_label, json_in, html_target in self.fixtures:
            if not skip_bs4:
                out_target = BeautifulSoup(html_target, "html5lib").prettify()
                out_actual = BeautifulSoup(
                    component_class(json_in).html, "html5lib"
                ).prettify()

            else:
                # BeautifulSoup can't parse invalid html, e.g. a <tr> tag not
                # inside a <table>. Use the flag `skip_bs4` to compare html
                # outputs without parsing first.
                out_target = html_target.replace(" ", "").replace("\n", "")
                out_actual = (
                    component_class(json_in)
                    .html.replace(" ", "")
                    .replace("\n", "")
                )

            try:
                self.assertEqual(
                    out_target,
                    out_actual,
                )
            except AssertionError as exc:
                print(
                    f"\nFailed: Component {component_class.__name__} "
                    f"fixture {fixture_label}"
                )
                raise exc
