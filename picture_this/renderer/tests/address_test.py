from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.address import AddressComponent


class AddressTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = AddressComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(AddressComponent)
