from unittest import TestCase

from renderer.tests._tester import TesterBase
from renderer.components.totals_table import TotalsTableComponent


class TotalsTableTestCase(TesterBase, TestCase):
    COMPONENT_LABEL = TotalsTableComponent.COMPONENT_LABEL

    def test_fixtures_loaded(self):
        self.assertGreater(len(self.fixtures), 0)

    def test_fixtures(self):
        super()._test_fixtures(TotalsTableComponent)
