import logging
from abc import ABC

from jinja2 import Environment, FileSystemLoader, select_autoescape

from settings import settings


logger = logging.getLogger(__name__)


class Component(ABC):
    COMPONENT_LABEL: str

    def __init__(self, data_in: dict) -> "Component":
        self._raw_data = data_in

        self.parsed_data = self.parse_data()

        self._template_path = (
            settings.RENDERER_BASE_DIR
            / "components"
            / "templates"
            / f"{self.COMPONENT_LABEL}.jinja"
        ).relative_to(settings.RENDERER_BASE_DIR)

        self._env = Environment(
            loader=FileSystemLoader(settings.RENDERER_BASE_DIR),
            autoescape=select_autoescape(),
        )

    @property
    def html(self):
        return self._template.render(**self.parsed_data)

    @property
    def options(self) -> list["Option"]:
        return self.get_choices()

    @classmethod
    def get_choices(cls):
        return [
            getattr(cls, attr)
            for attr in dir(cls)
            if attr != "options" and isinstance(getattr(cls, attr), Option)
        ]

    @property
    def _template(self):
        return self._env.get_template(str(self._template_path))

    @classmethod
    def render_docs(cls):
        docs = (
            COMPONENT_DOCS_TEMPLATE.format(
                COMPONENT_LABEL=cls.COMPONENT_LABEL, DESCRIPTION=cls.DESCRIPTION
            )
            + "\n"
        )
        docs += "\n\n".join(
            [option.render_option_docs() for option in cls.get_choices()]
        )
        return docs + "\n\n"

    def parse_data(self, manual_input: dict | None = None) -> dict:
        """Overwrite this method for custom parsing"""
        input_data = manual_input or self._raw_data
        parsed_data = {}

        for option in self.options:
            parsed_data.update(option.process(input_data.get(option.label)))

        return parsed_data


class Option(object):
    """Base class for component options."""

    def __init__(
        self,
        *_,
        label=None,
        help_text=None,
        choices=None,
        choice_mapper=None,
        jinja_variable=None,
        default=None,
        type=None,
        hidden=False,
    ):
        self.label = label
        self.help_text = help_text
        self.choices = choices
        self.choice_mapper = choice_mapper
        self.jinja_variable = jinja_variable or label
        self.default = default
        self.type = type
        self.hidden = hidden

    def process(self, value):
        """Implicit elses everywhere! Forgive me Jeebus."""
        if value is not None:
            if getattr(self, "choices", None) is not None:
                if value in self.choices:
                    if getattr(self, "choice_mapper", None) is not None:
                        return {self.jinja_variable: self.choice_mapper[value]}
                    return {self.jinja_variable: value}
                raise ValueError(f"Choice {value} not in {self.choices}")
            return {self.jinja_variable: value}
        if getattr(self, "default", None) is not None:
            if getattr(self, "choice_mapper", None) is not None:
                return {self.jinja_variable: self.choice_mapper[self.default]}
            return {self.jinja_variable: self.default}
        return {}

    def render_option_docs(self):
        if self.hidden:
            return ""

        return "\n".join(
            section
            for section in (
                self._title,
                self._help_text,
                self._type,
                self._choices,
                self._default,
            )
            if section is not None
        )

    @property
    def _title(self) -> str:
        return f"### `{self.label}`\n"

    @property
    def _help_text(self) -> str:
        return self.help_text + "\n" or None

    @property
    def _type(self) -> str:
        return (
            f"- Type: `{self.type}`".replace("typing.", "")
            .replace("<class '", "")
            .replace("'>", "")
        )

    @property
    def _choices(self) -> str:
        if self.choices:
            choices = [f"`{choice}`" for choice in self.choices]
            return f"- Choices: {', '.join(choices)}"

    @property
    def _default(self) -> str:
        return (
            f"- Default: `{self.default}`" if self.default is not None else None
        )


COMPONENT_DOCS_TEMPLATE = (
    """# Component `{COMPONENT_LABEL}`\n\n{DESCRIPTION}\n\n## Options:\n"""
)
