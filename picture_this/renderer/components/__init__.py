from typing import Union

from renderer.components.image import ImageComponent
from renderer.components.address import AddressComponent
from renderer.components.heading import HeadingComponent
from renderer.components.doc_info import DocInfoComponent
from renderer.components.block_text import BlockTextComponent
from renderer.components.table import TableComponent
from renderer.components.column_header import ColumnHeaderComponent
from renderer.components.page_break import PageBreakComponent
from renderer.components.totals_table import TotalsTableComponent
from renderer.components.footer import FooterComponent
from renderer.components.footer_column import FooterColumnComponent


ALL_COMPONENTS = (
    ImageComponent,
    AddressComponent,
    HeadingComponent,
    DocInfoComponent,
    BlockTextComponent,
    TableComponent,
    ColumnHeaderComponent,
    TotalsTableComponent,
    PageBreakComponent,
    FooterComponent,
    FooterColumnComponent,
)


AnyComponentType = Union[
    ImageComponent,
    AddressComponent,
    HeadingComponent,
    DocInfoComponent,
    BlockTextComponent,
    TableComponent,
    ColumnHeaderComponent,
    TotalsTableComponent,
    PageBreakComponent,
    FooterComponent,
    FooterColumnComponent,
]


MAPPER = {component.COMPONENT_LABEL: component for component in ALL_COMPONENTS}
