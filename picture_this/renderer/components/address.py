import logging

from renderer.components.base import Component, Option

logger = logging.getLogger(__name__)


class AddressComponent(Component):
    COMPONENT_LABEL = "address"
    DESCRIPTION = """Multiline address component with optional bold heading and optional right alignment."""

    header = Option(
        label="header",
        help_text="Top line in bold typeface",
        type=str,
    )

    lines = Option(
        label="lines",
        help_text="Address lines",
        type=list[str],
    )

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "",
            "middle": "align-middle",
            "right": "align-right",
        },
    )
