from renderer.components.base import Component, Option
from storage.backblaze_b2.b2_repository import create_repository


repository = create_repository()


class ImageComponent(Component):
    COMPONENT_LABEL = "image"
    DESCRIPTION = """Image component. Works with images and corresponding UUIDs uploaded via v1/upload."""

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="left",
        type=str,
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "",
            "middle": "align-middle",
            "right": "align-right",
        },
    )

    uuid = Option(
        label="uuid",
        help_text="UUID of image (supplied on file upload)",
        type=str,
    )

    def parse_data(self) -> dict:
        raw_data = self._raw_data.copy()
        parsed_data = super().parse_data()

        image_uuid = raw_data.pop("uuid")
        image_path = repository.download(image_uuid)

        parsed_data.update({"image_url": image_path})

        return parsed_data
