from pathlib import Path
from uuid import uuid4

from settings import settings
from weasyprint import HTML, CSS
from weasyprint.text.fonts import FontConfiguration

from renderer.components.base import Component, Option
from renderer.components import MAPPER as COMPONENT_MAPPER


class DocumentComponent(Component):
    COMPONENT_LABEL = "document"

    title = Option(
        label="title",
        help_text="PDF document title",
        type=str,
    )

    description = Option(
        label="description",
        help_text="PDF document metadata description",
        type=str,
    )

    return_as = Option(
        label="return_as",
        help_text='Return type from endpoint. "path" returns the path of the rendered file (e.g. within a shared Docker volume)',
        type=str,
        default="path",
        choices=("path", "pdf"),
    )

    def parse_data(self) -> dict:
        raw_data = self._raw_data.copy()

        components: list[Component] = []
        for component_dict in raw_data.get("components", []):
            component_label = component_dict.get("component")
            component_class = COMPONENT_MAPPER[component_label]
            components.append(component_class(component_dict))

        parsed_data = super().parse_data(raw_data)
        parsed_data.update({"components": components})

        return parsed_data

    def render(
        self,
        output_dir: Path = settings.PDF_OUTPUT_DIR,
        file_basename: str | None = None,
    ) -> Path:
        file_basename = file_basename or str(uuid4())

        output_path = output_dir / f"{file_basename}.pdf"
        font_config = FontConfiguration()

        HTML(string=self.html, base_url=str(settings.IMG_DIR)).write_pdf(
            target=output_path,
            stylesheets=[
                CSS(settings.RENDERER_STYLESHEET, font_config=font_config)
            ],
            font_config=font_config,
        )
        return output_path
