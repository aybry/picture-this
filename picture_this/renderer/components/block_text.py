import logging

from pydantic import BaseModel

from renderer.components.base import Component, Option

logger = logging.getLogger(__name__)


class BlockTextComponent(Component):
    COMPONENT_LABEL: str = "block_text"
    DESCRIPTION: str = """A block of text."""

    content = Option(
        label="content",
        help_text="Content of text block",
        type=str,
    )
