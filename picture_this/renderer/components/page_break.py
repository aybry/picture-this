import logging

from renderer.components.base import Component


logger = logging.getLogger(__name__)


class PageBreakComponent(Component):
    COMPONENT_LABEL = "page_break"
    DESCRIPTION = """Forces next component onto new page."""
