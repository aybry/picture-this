import logging

from renderer.components.base import Component, Option


logger = logging.getLogger(__name__)
TAG_CHOICES = ("h1", "h2", "h3", "h4", "h5", "h6")


class HeadingComponent(Component):
    COMPONENT_LABEL = "heading"
    DESCRIPTION = """A simple heading."""

    content = Option(
        label="content",
        help_text="Content of heading",
        type=str,
    )

    tag = Option(
        label="tag",
        help_text="Sets tag of heading node",
        default="h3",
        type=str,
        choices=TAG_CHOICES,
        choice_mapper={choice: choice for choice in TAG_CHOICES},
    )
