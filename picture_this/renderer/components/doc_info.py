import logging

from renderer.components.base import Component, Option

logger = logging.getLogger(__name__)


class DocInfoComponent(Component):
    COMPONENT_LABEL = "doc_info"
    DESCRIPTION = (
        "Block for listing document info (document number, date, etc.). "
        "Can render as plain plines or as table."
    )

    header = Option(
        label="header",
        help_text="Top line in bold typeface.",
        type=str,
    )

    lines = Option(
        label="lines",
        help_text="Info lines.",
        type=list[str],
    )

    table = Option(
        label="table",
        help_text="Info table defined by array of arrays of strings.",
        type=list[list[str]],
    )

    alignment = Option(
        label="alignment",
        help_text="Sets component alignment.",
        default="right",
        type=str,
        choices=("left", "middle", "right"),
        jinja_variable="alignment_class",
        choice_mapper={
            "left": "",
            "middle": "align-middle",
            "right": "align-right",
        },
    )
