from typing import Literal

from pydantic import BaseModel

from renderer.components import BlockTextComponent


class BlockText(BaseModel):
    component: Literal["block_text"]
    content: str

    def create_component(self) -> BlockTextComponent:
        return BlockTextComponent(self.model_dump())
