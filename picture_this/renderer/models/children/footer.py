from typing import Literal

from pydantic import BaseModel

from renderer.components import FooterComponent
from renderer.models.children.footer_column import FooterColumn


class Footer(BaseModel):
    component: Literal["footer"]
    columns: list[FooterColumn]

    def create_component(self) -> FooterComponent:
        return FooterComponent(self.model_dump())
