from typing import Literal

from pydantic import BaseModel

from renderer.components import FooterColumnComponent


class FooterColumn(BaseModel):
    lines: list[str] | None = None
    table: list[list[str]] | None = None
    alignment: Literal["left", "middle", "right"] = "left"
    justify_text: Literal["left", "middle", "right"] = "left"

    def create_component(self) -> FooterColumnComponent:
        return FooterColumnComponent(self.model_dump())
