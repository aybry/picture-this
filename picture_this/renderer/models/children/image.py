import uuid
from typing import Literal
from typing_extensions import Annotated

from pydantic import BaseModel
from pydantic.functional_validators import AfterValidator

from settings import settings
from renderer.components import ImageComponent
from storage.backblaze_b2.b2_repository import create_repository


repository = create_repository()


def validate_image_input(guid: str) -> str:
    guid_validated = validate_guid_is_valid(guid)
    validate_image_exists(guid_validated)
    return guid_validated


def validate_guid_is_valid(guid: str) -> str:
    try:
        guid_validated = uuid.UUID(guid, version=4)
    except ValueError:
        raise ValueError("Not a valid UUID")

    return str(guid_validated)


def validate_image_exists(guid_validated: str) -> str:
    try:
        image_path = repository.download(guid_validated)
    except:
        raise ValueError("Could not retrieve image")

    if not image_path.exists():
        raise ValueError("Image not found")

    return guid_validated


class Image(BaseModel):
    component: Literal["image"]
    alignment: Literal["left", "middle", "right"] = "left"
    uuid: Annotated[str, AfterValidator(validate_image_input)]

    def create_component(self) -> ImageComponent:
        return ImageComponent(self.model_dump())
