from typing import Literal

from pydantic import BaseModel

from renderer.components import PageBreakComponent


class PageBreak(BaseModel):
    component: Literal["page_break"]

    def create_component(self) -> PageBreakComponent:
        return PageBreakComponent(self.model_dump())
