from typing import Literal

from pydantic import BaseModel

from renderer.components import ColumnHeaderComponent


class ColumnHeader(BaseModel):
    content: str | None = None
    justify_text: Literal["left", "middle", "right"] = "left"
    data_type: Literal["text", "numeric", "currency"] = "text"
    fill_width: bool = False
    rowspan: bool = False

    def create_component(self) -> ColumnHeaderComponent:
        return ColumnHeaderComponent(self.model_dump())
