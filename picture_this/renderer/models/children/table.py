from typing import Literal

from pydantic import BaseModel

from renderer.components import TableComponent
from renderer.models.children.column_header import ColumnHeader


class Table(BaseModel):
    component: Literal["table"]
    column_headers: list[ColumnHeader]
    show_column_header_content: bool = True
    table_type: Literal[
        "data_table", "totals_table", "summary_table"
    ] = "data_table"
    rows: list[list[str]]
    alignment: Literal["left", "middle", "right"] = "middle"
    justify_text: Literal["left", "middle", "right"] = "left"
    fill_width: bool = True
    numbered_rows: bool = False
    numbered_row_label: str = "Item"
    numbered_row_template: str = "{idx}"
    colour_alternate_rows: bool = True

    def create_component(self) -> TableComponent:
        return TableComponent(self.model_dump())
