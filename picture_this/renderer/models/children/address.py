from typing import Literal

from pydantic import BaseModel

from renderer.components import AddressComponent


class Address(BaseModel):
    component: Literal["address"]
    header: str | None = None
    lines: list[str]
    alignment: Literal["left", "middle", "right"] = "left"

    def create_component(self) -> AddressComponent:
        return AddressComponent(self.model_dump())
