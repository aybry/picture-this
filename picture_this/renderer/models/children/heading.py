from typing import Literal

from pydantic import BaseModel

from renderer.components import HeadingComponent


class Heading(BaseModel):
    component: Literal["heading"]
    content: str
    tag: Literal["h1", "h2", "h3", "h4", "h5", "h6"] = "h3"

    def create_component(self) -> HeadingComponent:
        return HeadingComponent(self.model_dump())
