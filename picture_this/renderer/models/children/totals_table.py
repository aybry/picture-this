from typing import Literal

from pydantic import BaseModel

from renderer.components import TotalsTableComponent


class TotalsTable(BaseModel):
    component: Literal["totals_table"]
    rows: list[list[str]]
    alignment: Literal["left", "middle", "right"] = "right"

    def create_component(self) -> TotalsTableComponent:
        return TotalsTableComponent(self.model_dump())
