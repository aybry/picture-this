from typing import Literal

from pydantic import BaseModel

from renderer.components import DocInfoComponent


class DocInfo(BaseModel):
    component: Literal["doc_info"]
    header: str | None = None
    lines: list[str] | None = None
    table: list[list[str]] | None = None
    alignment: Literal["left", "middle", "right"] = "right"

    def create_component(self) -> DocInfoComponent:
        return DocInfoComponent(self.model_dump())
