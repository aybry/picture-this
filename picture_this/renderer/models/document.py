import logging
from typing import Literal
from typing_extensions import Annotated

from pydantic import BaseModel, Discriminator, Tag

from renderer.components.document import DocumentComponent
from renderer.models.children import (
    Address,
    BlockText,
    DocInfo,
    Footer,
    Heading,
    Image,
    PageBreak,
    Table,
    TotalsTable,
)

logger = logging.getLogger(__name__)


def get_discriminator_value(dto: dict) -> str:
    return dto.get("component")


class Document(BaseModel):
    title: str
    description: str
    return_as: Literal["path", "pdf"] = "pdf"
    components: list[
        Annotated[
            Annotated[Address, Tag("address")]
            | Annotated[BlockText, Tag("block_text")]
            | Annotated[DocInfo, Tag("doc_info")]
            | Annotated[Footer, Tag("footer")]
            | Annotated[Heading, Tag("heading")]
            | Annotated[Image, Tag("image")]
            | Annotated[PageBreak, Tag("page_break")]
            | Annotated[Table, Tag("table")]
            | Annotated[TotalsTable, Tag("totals_table")],
            Discriminator(get_discriminator_value),
        ]
    ]

    def create_component(self) -> DocumentComponent:
        return DocumentComponent(self.model_dump())
