import json
import requests
from pathlib import Path

input_filename = "sandbox.json"

RENDER_ENDPOINT_URL = "https://picture-this.ay-bryson.com/v1/render"
EXAMPLES_DIR = Path("picture_this") / "v1" / "docs" / "examples"
TEMPLATES_DIR = EXAMPLES_DIR / "templates"

input_filepath = TEMPLATES_DIR / input_filename
output_filepath = EXAMPLES_DIR / "output.pdf"

with input_filepath.open() as f:
    pt_json = json.load(f)

res = requests.post(RENDER_ENDPOINT_URL, json=pt_json, timeout=10)
with open(output_filepath, "wb+") as f:
    f.write(res.content)

pass
