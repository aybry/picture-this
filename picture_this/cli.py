import click
from actions import _create_docs


@click.group()
def cli():
    pass


@click.command()
def create_docs():
    _create_docs.main()


cli.add_command(create_docs)


if __name__ == "__main__":
    cli()
