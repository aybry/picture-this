from abc import ABC, abstractmethod


class IRepository(ABC):
    @abstractmethod
    def create_resource(self):
        pass

    @abstractmethod
    def download(self):
        pass

    @abstractmethod
    def upload(self):
        pass
