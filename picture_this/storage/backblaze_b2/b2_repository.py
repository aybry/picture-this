import io
import tempfile
from copy import deepcopy
from pathlib import Path

import boto3
from PIL import Image
from botocore.config import Config
from mypy_boto3_s3.client import S3Client

from settings import settings
from storage.repository import IRepository


class B2Repository(IRepository):
    def create_resource(self) -> S3Client:
        b2_resource = boto3.resource(
            service_name="s3",
            endpoint_url=settings.B2_BUCKET_ENDPOINT,
            aws_access_key_id=settings.B2_APPLICATION_KEY_ID,
            aws_secret_access_key=settings.B2_APPLICATION_KEY,
            config=Config(signature_version="s3v4"),
        )
        self.bucket = b2_resource.Bucket(settings.B2_BUCKET_NAME)

    def upload(self, file_obj: io.BytesIO, key: str) -> None:
        file_obj_copy = deepcopy(file_obj)
        Image.open(file_obj_copy)
        self.bucket.upload_fileobj(file_obj, key)

    def download(self, key: str, file_path: Path | None = None) -> Path:
        if file_path is None:
            file_path = Path(tempfile.gettempdir()) / key

        if file_path.exists():
            return file_path

        self.bucket.download_file(key, file_path)
        Image.open(file_path)

        return file_path

    def delete(self, key: str) -> None:
        self.bucket.delete_objects(Delete={"Objects": [{"Key": key}]})


def create_repository() -> B2Repository:
    repository = B2Repository()
    repository.create_resource()
    return repository
