import io
import tempfile
import uuid
from pathlib import Path

from settings import settings
from storage.backblaze_b2.b2_repository import B2Repository


# def test_b2_repository_uploads_downloads():
#     repository = B2Repository()
#     repository.create_resource()

#     filename_uuid = str(uuid.uuid4())
#     image_file_path = settings.IMG_DIR / "ab-favicon.png"
#     assert image_file_path.exists()
#     with image_file_path.open("rb") as f:
#         repository.upload(f, filename_uuid)

#     file_path = Path(tempfile.gettempdir()) / f"{filename_uuid}"
#     assert not file_path.exists()
#     repository.download(filename_uuid, file_path=file_path)
#     assert file_path.exists()
