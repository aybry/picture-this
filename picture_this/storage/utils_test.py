import tempfile
from pathlib import Path

from storage.utils import create_file_temporarily


def test_temp_file_context_manager():
    temp_file_path = Path(tempfile.gettempdir()) / "test.txt"
    assert not temp_file_path.exists()

    with create_file_temporarily(temp_file_path):
        assert temp_file_path.exists()

    assert not temp_file_path.exists()
