from pathlib import Path
from contextlib import contextmanager


@contextmanager
def create_file_temporarily(absolute_path: Path):
    try:
        absolute_path.touch()
        yield
    finally:
        absolute_path.unlink()
