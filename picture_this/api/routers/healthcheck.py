import requests
import sys

from fastapi import APIRouter


router = APIRouter()


@router.get("/healthcheck")
async def healthcheck():
    return {"message": "All is well!"}


def check_health():
    url = "http://localhost:8000/healthcheck"
    try:
        requests.get(url, timeout=10).raise_for_status()
    except Exception:
        sys.exit(1)


if __name__ == "__main__":
    check_health()
