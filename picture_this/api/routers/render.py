from fastapi import APIRouter
from fastapi.responses import FileResponse

from renderer.models.document import Document


router = APIRouter()


@router.post("/render")
async def render(document_validated: Document):
    document = document_validated.create_component()
    pdf_path = document.render()
    if document_validated.return_as == "path":
        return {"path": pdf_path}
    return FileResponse(pdf_path)
