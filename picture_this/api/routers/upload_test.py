import uuid

from fastapi.testclient import TestClient

from api.main import app
from settings import settings
from storage.backblaze_b2.b2_repository import create_repository


client = TestClient(app)
repository = create_repository()


def test_upload():
    image_to_upload = settings.BASE_DIR / "static" / "img" / "ab-favicon.png"

    with image_to_upload.open("rb") as img_f:
        response = client.post(
            "/upload", files={"image_file": ("filename", img_f, "image/png")}
        )

    res = response.json()
    assert response.status_code == 200
    assert res["status"] == "ok"
    assert res["file_size"] == 41662
    assert uuid.UUID(res["file_key"])

    repository.delete(res["file_key"])
