from fastapi.testclient import TestClient

from api.main import app
from api.helpers.render_helpers import (
    create_document_base_data,
    create_document_dict_from_component_dict,
)


client = TestClient(app)


def test_empty_document():
    response = client.request(
        method="POST", url="/render", json=create_document_base_data()
    )
    assert response.status_code == 200


def test_address_valid():
    address_data = {
        "component": "address",
        "header": "Test Header",
        "lines": ["Test line 1", "Test line 2"],
        "alignment": "right",
    }
    data = create_document_dict_from_component_dict(address_data)
    response = client.request(method="POST", url="/render", json=data)
    assert response.status_code == 200


def test_image_validation_fails_on_invalid_guid():
    image_data = {
        "component": "image",
        "uuid": "not-a-valid-guid",
    }
    data = create_document_dict_from_component_dict(image_data)
    response = client.request(method="POST", url="/render", json=data)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "Value error, Not a valid UUID"


def test_image_validation_fails_on_nonexistent_image():
    image_data = {
        "component": "image",
        "uuid": "cbf953fd-ddff-4f7e-9cf7-7e3edf45b50d",
    }
    data = create_document_dict_from_component_dict(image_data)
    response = client.request(method="POST", url="/render", json=data)
    assert response.status_code == 422
    assert (
        response.json()["detail"][0]["msg"]
        == "Value error, Could not retrieve image"
    )


def test_image_validation_succeeds_on_valid_guid():
    image_data = {
        "component": "image",
        "uuid": "33f78f17-e0f8-4b1e-8fe5-85369107d56d",
    }
    data = create_document_dict_from_component_dict(image_data)
    response = client.request(method="POST", url="/render", json=data)
    assert response.status_code == 200
