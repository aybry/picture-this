import tempfile
import uuid
from pathlib import Path

from fastapi import APIRouter, UploadFile
from mypy_boto3_s3.client import S3Client

from storage.backblaze_b2.b2_repository import create_repository


router = APIRouter()
upload_repository = create_repository()


@router.post("/upload")
def upload(image_file: UploadFile):
    file_key = str(uuid.uuid4())

    upload_repository.upload(image_file.file, file_key)

    return {"status": "ok", "file_key": file_key, "file_size": image_file.size}
