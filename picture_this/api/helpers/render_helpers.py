from typing import TypedDict


class DocumentData(TypedDict):
    title: str
    description: str
    return_as: str
    components: list


def create_document_base_data() -> DocumentData:
    return DocumentData(
        title="Document Title",
        description="Document description",
        return_as="path",
        components=[],
    )


def create_document_dict_from_component_dict(
    component_dict: dict,
) -> DocumentData:
    data: DocumentData = create_document_base_data()
    data["components"].append(component_dict)
    return data
