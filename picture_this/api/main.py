from fastapi import FastAPI

from api.routers import healthcheck, render, upload
from settings import settings


def ensure_data_dirs_exist():
    for data_dir in [
        settings.PT_DATA_DIR,
        settings.IMG_DIR,
        settings.PDF_OUTPUT_DIR,
    ]:
        data_dir.mkdir(parents=True, exist_ok=True)


ensure_data_dirs_exist()

app = FastAPI()

app.include_router(healthcheck.router)
app.include_router(render.router)
app.include_router(upload.router)
