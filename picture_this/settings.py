import os
from pathlib import Path

from pydantic_settings import BaseSettings


class settings(BaseSettings):
    _instance: BaseSettings | None = None

    BASE_DIR: Path = Path(__file__).resolve().parent

    RENDERER_BASE_DIR: Path = BASE_DIR / "renderer"
    RENDERER_STYLESHEET: Path = (
        RENDERER_BASE_DIR / "static" / "renderer" / "css" / "style.css"
    )

    PT_DATA_DIR: Path = BASE_DIR.parent / "pt_data"
    PDF_OUTPUT_DIR: Path = PT_DATA_DIR
    IMG_DIR: Path = PT_DATA_DIR / "img"

    B2_BUCKET_NAME: str = os.environ.get("B2_BUCKET_NAME", "")
    B2_BUCKET_ENDPOINT: str = os.environ.get("B2_BUCKET_ENDPOINT", "")
    B2_APPLICATION_KEY_ID: str = os.environ.get("B2_APPLICATION_KEY_ID", "")
    B2_APPLICATION_KEY: str = os.environ.get("B2_APPLICATION_KEY", "")

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            instance = super().__call__(*args, **kwargs)
            cls._instance = instance
        return cls._instance


settings = settings()
