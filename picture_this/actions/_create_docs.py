import logging

from renderer.components import MAPPER
from renderer.components.base import Component
from settings import settings


logger = logging.getLogger(__name__)


DOC_OUTPUT_FILE = settings.RENDERER_BASE_DIR / "docs" / "components.md"
ACTIVE_COMPONENTS: list[Component] = [
    component for _, component in MAPPER.items()
]


def main():
    autogenerated_warning = (
        "<!-- This document is autogenerated! Do not edit manually! -->\n\n"
    )
    docs_content = autogenerated_warning

    for component_class in ACTIVE_COMPONENTS:
        docs_content += component_class.render_docs()
        docs_content += autogenerated_warning

    with DOC_OUTPUT_FILE.open("w+") as f:
        f.write(docs_content)
